<?php

return [
    'show_more_button' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options'       => [
                'yes' => 'Yes',
                'no'  => 'No',
            ],
            'default_value' => 'yes',
        ]
    ],
    'sort_order'       => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options'       => [
                'title'      => 'Title',
                'created_at' => 'Created at',
                'updated_at' => 'Updated at',
                'order'      => 'Sort order'
            ],
            'default_value' => 'order',
        ]
    ],
    'project_link'     => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options'       => [
                'yes' => 'Yes',
                'no'  => 'No',
            ],
            'default_value' => 'no',
        ],
    ],
    'thumbnail_size'   => [
        'type'   => 'anomaly.field_type.integer',
        'config' => [
            'default_value' => '195',
        ],
    ],
];
