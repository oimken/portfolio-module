<?php

namespace Newebtime\PortfolioProjectsBlockExtension;

use Anomaly\BlocksModule\Block\BlockExtension;

/**
 * Class PortfolioProjectsBlockExtension
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioProjectsBlockExtension extends BlockExtension
{
    /**
     * {@inheritdoc}
     */
    protected $provides = 'anomaly.module.blocks::block.portfolio_projects';

    /**
     * {@inheritdoc}
     */
    protected $view = 'newebtime.extension.portfolio_projects_block::content';
}
