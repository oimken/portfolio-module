<?php

return [
    'title' => [
        'name' => 'Titre'
    ],
    'project' => [
        'name' => 'Projet'
    ],
    'description' => [
        'name' => 'Description'
    ],
];
