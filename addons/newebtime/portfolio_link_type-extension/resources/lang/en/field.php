<?php

return [
    'title' => [
        'name' => 'Title'
    ],
    'project' => [
        'name' => 'Project'
    ],
    'description' => [
        'name' => 'Description'
    ],
];
