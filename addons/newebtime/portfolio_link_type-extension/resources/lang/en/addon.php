<?php

return [
    'title'       => 'Portfolio',
    'name'        => 'Portfolio Link Type Extension',
    'description' => 'A portfolio link type for the Navigation module.'
];
