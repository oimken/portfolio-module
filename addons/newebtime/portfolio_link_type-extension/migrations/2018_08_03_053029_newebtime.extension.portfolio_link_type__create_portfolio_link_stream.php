<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class NewebtimeExtensionPortfolioLinkTypeCreatePortfolioLinkStream extends Migration
{
    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'portfolio_link',
        'title_column' => 'title',
        'translatable' => true,
        'trashable'    => false,
        'searchable'   => false,
        'sortable'     => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title'       => [
            'translatable' => true,
        ],
        'project'     => [
            'required' => true,
        ],
        'description' => [
            'translatable' => true,
        ],
    ];
}
