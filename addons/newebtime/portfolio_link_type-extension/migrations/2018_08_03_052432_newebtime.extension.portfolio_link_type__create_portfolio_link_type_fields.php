<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Newebtime\PortfolioModule\Project\ProjectModel;

class NewebtimeExtensionPortfolioLinkTypeCreatePortfolioLinkTypeFields extends Migration
{
    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'       => 'anomaly.field_type.text',
        'project'     => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'mode'    => 'lookup',
                'related' => ProjectModel::class,
            ],
        ],
        'description' => 'anomaly.field_type.textarea',
    ];
}
