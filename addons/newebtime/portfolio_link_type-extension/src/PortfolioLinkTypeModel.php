<?php

namespace Newebtime\PortfolioLinkTypeExtension;

use Newebtime\PortfolioLinkTypeExtension\Contract\PortfolioLinkTypeInterface;
use Anomaly\Streams\Platform\Model\PortfolioLinkType\PortfolioLinkTypePortfolioLinkEntryModel;

/**
 * Class PortfolioLinkTypeModel
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioLinkTypeModel extends PortfolioLinkTypePortfolioLinkEntryModel implements PortfolioLinkTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function getProject()
    {
        return $this->project;
    }
}
