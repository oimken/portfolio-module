<?php

namespace Newebtime\PortfolioLinkTypeExtension\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class PortfolioLinkTypeFormBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioLinkTypeFormBuilder extends FormBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'cancel',
    ];
}
