<?php

namespace Newebtime\PortfolioLinkTypeExtension\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Newebtime\PortfolioModule\Project\Contract\ProjectInterface;

/**
 * Interface PortfolioLinkTypeInterface
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
interface PortfolioLinkTypeInterface extends EntryInterface
{
    /**
     * Get the project.
     *
     * @return ProjectInterface
     */
    public function getProject();
}
