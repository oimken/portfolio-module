<?php

namespace Newebtime\PortfolioLinkTypeExtension;

use Anomaly\NavigationModule\Link\Contract\LinkInterface;
use Anomaly\NavigationModule\Link\Type\Contract\LinkTypeInterface;
use Anomaly\NavigationModule\Link\Type\LinkTypeExtension;
use Newebtime\PortfolioLinkTypeExtension\Form\PortfolioLinkTypeFormBuilder;
use Newebtime\PortfolioModule\Project\Contract\ProjectInterface;

/**
 * Class PortfolioLinkTypeExtension
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioLinkTypeExtension extends LinkTypeExtension implements LinkTypeInterface
{
    /**
     * {@inheritdoc}
     */
    protected $provides = 'anomaly.module.navigation::link_type.project';

    /**
     * {@inheritdoc}
     */
    public function url(LinkInterface $link)
    {
        /* @var PortfolioLinkTypeModel $entry */
        $entry = $link->getEntry();

        if (!$project = $entry->getProject()) {
            return url('');
        }

        return route('newebtime.module.portfolio::projects.show', [
            'projectSlug' => $project->slug,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function title(LinkInterface $link)
    {
        /* @var PortfolioLinkTypeModel $entry */
        $entry = $link->getEntry();

        if (!$project = $entry->getProject()) {
            return '[Broken Link]';
        }

        return $entry->getTitle() ?: $project->getTitle();
    }

    /**
     * {@inheritdoc}
     */
    public function exists(LinkInterface $link)
    {
        /* @var PortfolioLinkTypeModel $entry */
        $entry = $link->getEntry();

        return (bool)$entry->getProject();
    }

    /**
     * {@inheritdoc}
     */
    public function enabled(LinkInterface $link)
    {
        /* @var PortfolioLinkTypeModel $entry */
        if (!$entry = $link->getEntry()) {
            return false;
        }

        /* @var ProjectInterface $project */
        if (!$project = $entry->getProject()) {
            return false;
        }

        return $project->isEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function builder()
    {
        return app(PortfolioLinkTypeFormBuilder::class);
    }
}
