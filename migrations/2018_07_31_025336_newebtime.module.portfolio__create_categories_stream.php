<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class NewebtimeModulePortfolioCreateCategoriesStream extends Migration
{
    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
         'slug'         => 'categories',
         'title_column' => 'name',
         'translatable' => true,
         'trashable'    => false,
         'searchable'   => false,
         'sortable'     => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'         => [
            'translatable' => true,
            'required'     => true,
            'unique'       => true,
        ],
        'slug' => [
            'unique'   => true,
            'required' => true,
        ],
        'description' => [
            'translatable' => true,
        ],
        'theme_layout' => [
            'required' => true,
        ],
        'layout',
    ];
}
