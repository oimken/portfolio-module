<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class NewebtimeModulePortfolioCreateServicesStream extends Migration
{
    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'services',
        'title_column' => 'title',
        'translatable' => true,
        'trashable'    => true,
        'searchable'   => false,
        'sortable'     => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title' => [
            'required'     => true,
            'translatable' => true,
        ],
        'slug'  => [
            'unique'   => true,
            'required' => true,
        ],
        'enabled',
        'parent',
        'description' => [
            'translatable' => true,
        ],
        'page'
    ];
}
