<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class NewebtimeModulePortfolioCreateProjectsStream extends Migration
{
    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'projects',
        'title_column' => 'title',
        'translatable' => true,
        'trashable'    => true,
        'searchable'   => false,
        'sortable'     => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title'  => [
            'required' => true,
        ],
        'slug'   => [
            'unique'   => true,
            'required' => true,
        ],
        'enabled',
        'description'  => [
            'translatable' => true,
        ],
        'headline',
        'industry',
        'website',
        'date',
        'client' => [
            'required' => true,
        ],
        'services',
        'images',
        'order',
        'category',
        'theme_layout',
        'meta_title',
        'meta_description' => [
            'translatable' => true,
        ],
    ];
}
