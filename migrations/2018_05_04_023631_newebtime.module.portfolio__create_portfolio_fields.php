<?php

use Anomaly\PagesModule\Page\PageModel;
use Anomaly\Streams\Platform\Database\Migration\Migration;
use Newebtime\PortfolioModule\Category\CategoryModel;
use Newebtime\PortfolioModule\Client\ClientModel;
use Newebtime\PortfolioModule\Service\ServiceModel;

class NewebtimeModulePortfolioCreatePortfolioFields extends Migration
{
    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'       => 'anomaly.field_type.text',
        'slug'        => [
            'type'   => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'title',
                'type'    => '-'
            ],
        ],
        'description' => 'anomaly.field_type.wysiwyg',
        'industry'    => 'anomaly.field_type.text',
        'website'     => 'anomaly.field_type.text',
        'date'        => 'anomaly.field_type.datetime',
        'client'      => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'title_name' => 'title',
                'related'    => ClientModel::class,
            ],
        ],
        'services'    => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'title_name' => 'title',
                'related'    => ServiceModel::class,
            ],
        ],
        'images'      => [
            'type' => 'anomaly.field_type.files',
        ],
        'enabled'     => 'anomaly.field_type.boolean',
        'order'       => 'anomaly.field_type.integer',
        'name'        => 'anomaly.field_type.text',
        'theme_layout'     => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'default_value' => 'theme::layouts/default.twig',
                'handler'       => 'Anomaly\SelectFieldType\Handler\Layouts@handle',
            ],
        ],
        'layout'           => [
            'type'   => 'anomaly.field_type.editor',
            'config' => [
                'default_value' => '<h1>{{ project.title }}</h1>',
                'mode'          => 'twig',
            ],
        ],
        'parent'        => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => 'Newebtime\PortfolioModule\Service\ServiceModel',
            ],
        ],
        'category' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'title_name' => 'name',
                'related'    => CategoryModel::class,
            ],
        ],
        'logo'      => [
            'type' => 'anomaly.field_type.files',
        ],
        'page'      => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'title_name' => 'title',
                'related'    => PageModel::class,
                'mode'       => 'lookup'
            ],
        ],
        'meta_title'       => 'anomaly.field_type.text',
        'meta_description' => 'anomaly.field_type.textarea',
        'headline'         => 'anomaly.field_type.text',
    ];
}
