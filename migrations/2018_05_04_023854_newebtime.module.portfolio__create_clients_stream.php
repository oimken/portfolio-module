<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class NewebtimeModulePortfolioCreateClientsStream extends Migration
{
    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'clients',
        'title_column' => 'title',
        'translatable' => false,
        'trashable'    => true,
        'searchable'   => false,
        'sortable'     => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title' => [
            'required' => true,
        ],
        'slug'  => [
            'unique'   => true,
            'required' => true,
        ],
        'enabled',
        'description',
        'logo'
    ];
}
