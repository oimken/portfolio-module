<?php

namespace Newebtime\PortfolioModule\Service\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class ServiceFormBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceFormBuilder extends FormBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'cancel',
    ];
}
