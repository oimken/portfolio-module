<?php

namespace Newebtime\PortfolioModule\Service\Tree;

use Newebtime\PortfolioModule\Service\Contract\ServiceInterface;

/**
 * Class ServiceTreeSegments
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceTreeSegments
{
    /**
     * Handle the tree segments.
     *
     * @param ServiceTreeBuilder $builder
     */
    public function handle(ServiceTreeBuilder $builder)
    {
        $builder->setSegments(
            [
                'entry.edit_link',
                [
                    'class' => 'text-faded',
                    'value' => function (ServiceInterface $entry) {
                        return '<span class="small" style="padding-right:10px;">' . $entry->getTitle() . '</span>';
                    },
                ],
                [
                    'data-toggle' => 'tooltip',
                    'class'       => 'text-danger',
                    'value'       => '<i class="fa fa-ban"></i>',
                    'attributes'  => [
                        'title' => 'module::message.disabled',
                    ],
                    'enabled'     => function (ServiceInterface $entry) {
                        return !$entry->isEnabled();
                    },
                ],
            ]
        );
    }
}
