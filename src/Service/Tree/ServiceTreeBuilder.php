<?php

namespace Newebtime\PortfolioModule\Service\Tree;

use Anomaly\Streams\Platform\Ui\Tree\TreeBuilder;

/**
 * Class ServiceTreeBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceTreeBuilder extends TreeBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'view' => [
            'target' => '_blank',
        ],
        'delete' => [
            'permission' => 'newebtime.module.portfolio::services.delete'
        ],
    ];
}
