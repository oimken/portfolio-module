<?php

namespace Newebtime\PortfolioModule\Service\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface ServiceInterface
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
interface ServiceInterface extends EntryInterface
{
    /**
     * Get the enabled flag.
     *
     * @return bool
     */
    public function isEnabled();
}
