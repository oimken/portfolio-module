<?php

namespace Newebtime\PortfolioModule\Service\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Interface ServiceRepositoryInterface
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
interface ServiceRepositoryInterface extends EntryRepositoryInterface
{

}
