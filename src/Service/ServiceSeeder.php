<?php

namespace Newebtime\PortfolioModule\Service;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class ServiceSeeder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceSeeder extends Seeder
{
    /**
     * @var ServiceRepository
     */
    protected $services;

    /**
     * ServiceSeeder constructor.
     *
     * @param ServiceRepository $services
     */
    public function __construct(ServiceRepository $services)
    {
        $this->services = $services;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->services->create(
            [
                'title'   => 'Design',
                'slug'    => 'design',
                'enabled' => true
            ]
        );
        $this->services->create(
            [
                'title'   => 'Art Direction',
                'slug'    => 'art_direction',
                'enabled' => true
            ]
        );
        $this->services->create(
            [
                'title'   => 'Website',
                'slug'    => 'website',
                'enabled' => true
            ]
        );
    }
}
