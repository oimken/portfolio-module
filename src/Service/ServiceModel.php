<?php

namespace Newebtime\PortfolioModule\Service;

use Newebtime\PortfolioModule\Service\Contract\ServiceInterface;
use Anomaly\Streams\Platform\Model\Portfolio\PortfolioServicesEntryModel;

/**
 * Class ServiceModel
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceModel extends PortfolioServicesEntryModel implements ServiceInterface
{
    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
}
