<?php

namespace Newebtime\PortfolioModule\Service\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class ServiceTableBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceTableBuilder extends TableBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $filters = [
        'search' => [
            'fields' => [
                'title'
            ]
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'edit',
        'view' => [
            'target' => '_blank',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected $actions = [
        'delete'
    ];
}
