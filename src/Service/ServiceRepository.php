<?php

namespace Newebtime\PortfolioModule\Service;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Newebtime\PortfolioModule\Service\Contract\ServiceRepositoryInterface;

/**
 * Class ServiceRepository
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceRepository extends EntryRepository implements ServiceRepositoryInterface
{
    /**
     * @var ServiceModel
     */
    protected $model;

    /**
     * Create a new ServiceRepository instance.
     *
     * @param ServiceModel $model
     */
    public function __construct(ServiceModel $model)
    {
        $this->model = $model;
    }
}
