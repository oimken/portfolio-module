<?php

namespace Newebtime\PortfolioModule\Service;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class ServiceCollection
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceCollection extends EntryCollection
{
    /**
     * Render the blocks.
     *
     * @return string
     */
    public function render()
    {
        return implode(
            "\n\n",
            $this->labels()
        );
    }
}
