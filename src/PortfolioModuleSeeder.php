<?php

namespace Newebtime\PortfolioModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Newebtime\PortfolioModule\Category\CategorySeeder;
use Newebtime\PortfolioModule\Client\ClientSeeder;
use Newebtime\PortfolioModule\Project\ProjectSeeder;
use Newebtime\PortfolioModule\Service\ServiceSeeder;

/**
 * Class PortfolioModuleSeeder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioModuleSeeder extends Seeder
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->call(ClientSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(CategorySeeder::class);
    }
}
