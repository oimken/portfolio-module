<?php

namespace Newebtime\PortfolioModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Newebtime\PortfolioModule\Category\Contract\CategoryRepositoryInterface;
use Newebtime\PortfolioModule\Category\CategoryRepository;
use Anomaly\Streams\Platform\Model\Portfolio\PortfolioCategoriesEntryModel;
use Newebtime\PortfolioModule\Category\CategoryModel;
use Anomaly\Streams\Platform\Assignment\AssignmentRouter;
use Anomaly\Streams\Platform\Field\FieldRouter;
use Anomaly\Streams\Platform\Model\Portfolio\PortfolioClientsEntryModel;
use Anomaly\Streams\Platform\Model\Portfolio\PortfolioProjectsEntryModel;
use Anomaly\Streams\Platform\Model\Portfolio\PortfolioServicesEntryModel;
use Newebtime\PortfolioModule\Client\Contract\ClientRepositoryInterface;
use Newebtime\PortfolioModule\Client\ClientRepository;
use Newebtime\PortfolioModule\Client\ClientModel;
use Newebtime\PortfolioModule\Http\Controller\Admin\AssignmentsController;
use Newebtime\PortfolioModule\Http\Controller\Admin\FieldsController;
use Newebtime\PortfolioModule\Project\Contract\ProjectRepositoryInterface;
use Newebtime\PortfolioModule\Project\ProjectRepository;
use Newebtime\PortfolioModule\Project\ProjectModel;
use Newebtime\PortfolioModule\Service\Contract\ServiceRepositoryInterface;
use Newebtime\PortfolioModule\Service\ServiceRepository;
use Newebtime\PortfolioModule\Service\ServiceModel;

/**
 * Class PortfolioModuleServiceProvider
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioModuleServiceProvider extends AddonServiceProvider
{
    /**
     * {@inheritdoc}
     */
    protected $routes = [
        /* Frontend */
        'portfolio/client/{customerSlug?}' => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\CustomersController@index',
            'as'   => 'newebtime.module.portfolio::customers.index',
        ],
        'portfolio/{serviceSlug?}'         => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\ServicesController@index',
            'as'   => 'newebtime.module.portfolio::services.index',
        ],
        'portfolio/project/{projectSlug}'  => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\ProjectsController@show',
            'as'   => 'newebtime.module.portfolio::projects.show',
        ],

        /* Backend */
        'admin/portfolio/services'             => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ServicesController@index',
        ],
        'admin/portfolio/services/change/{id}' => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ServicesController@change',
        ],
        'admin/portfolio/services/create'      => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ServicesController@create',
        ],
        'admin/portfolio/services/edit/{id}'   => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ServicesController@edit',
        ],
        'admin/portfolio/services/view/{id}'   => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ServicesController@view',
        ],
        'admin/portfolio'                      => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ProjectsController@index',
        ],
        'admin/portfolio/create'               => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ProjectsController@create',
        ],
        'admin/portfolio/edit/{id}'            => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ProjectsController@edit',
        ],
        'admin/portfolio/view/{id}'            => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ProjectsController@view',
        ],
        'admin/portfolio/clients'              => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ClientsController@index',
        ],
        'admin/portfolio/clients/create'       => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ClientsController@create',
        ],
        'admin/portfolio/clients/edit/{id}'    => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ClientsController@edit',
        ],
        'admin/portfolio/clients/view/{id}'    => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\ClientsController@view',
        ],
        'admin/portfolio/categories'           => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\CategoriesController@index',
        ],
        'admin/portfolio/categories/create'    => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\CategoriesController@create',
        ],
        'admin/portfolio/categories/edit/{id}' => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\CategoriesController@edit',
        ],
        'admin/portfolio/settings'             => [
            'uses' => 'Newebtime\PortfolioModule\Http\Controller\Admin\SettingsController@edit',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected $bindings = [
        PortfolioCategoriesEntryModel::class => CategoryModel::class,
        PortfolioServicesEntryModel::class   => ServiceModel::class,
        PortfolioProjectsEntryModel::class   => ProjectModel::class,
        PortfolioClientsEntryModel::class    => ClientModel::class,
    ];

    /**
     * {@inheritdoc}
     */
    protected $singletons = [
        CategoryRepositoryInterface::class => CategoryRepository::class,
        ServiceRepositoryInterface::class  => ServiceRepository::class,
        ProjectRepositoryInterface::class  => ProjectRepository::class,
        ClientRepositoryInterface::class   => ClientRepository::class,
    ];

    /**
     * Map additional addon routes.
     *
     * @param FieldRouter      $fields
     * @param AssignmentRouter $assignments
     */
    public function map(FieldRouter $fields, AssignmentRouter $assignments)
    {
        $fields->route($this->addon, FieldsController::class);
        $assignments->route($this->addon, AssignmentsController::class, 'admin/portfolio');
    }
}
