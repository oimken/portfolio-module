<?php

namespace Newebtime\PortfolioModule;

use Anomaly\Streams\Platform\Addon\Module\Module;
use Newebtime\PortfolioLinkTypeExtension\PortfolioLinkTypeExtension;
use Newebtime\PortfolioProjectsBlockExtension\PortfolioProjectsBlockExtension;

/**
 * Class PortfolioModule
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioModule extends Module
{
    /**
     * {@inheritdoc}
     */
    protected $addons = [
        PortfolioLinkTypeExtension::class,
        PortfolioProjectsBlockExtension::class,
    ];

    /**
     * {@inheritdoc}
     */
    protected $icon = 'fa fa-book';
}
