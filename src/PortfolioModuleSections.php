<?php

namespace Newebtime\PortfolioModule;

use Anomaly\PreferencesModule\Preference\Contract\PreferenceRepositoryInterface;
use Anomaly\Streams\Platform\Ui\ControlPanel\ControlPanelBuilder;
use Newebtime\PortfolioModule\Project\ProjectModel;

/**
 * Class PortfolioModuleSections
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PortfolioModuleSections
{
    /**
     * Handle the sections.
     *
     * @param ControlPanelBuilder           $builder
     * @param PreferenceRepositoryInterface $preferences
     */
    public function handle($builder, PreferenceRepositoryInterface $preferences)
    {
        $view = $preferences->value('newebtime.module.portfolio::page_view', 'tree');

        $builder->setSections([
            'projects' => [
                'buttons' => [
                    'new_project',
                    'assignments'  => [
                        'href' => function (ProjectModel $entry) {
                            return '/admin/portfolio/assignments/' . $entry->getEntryStreamId();
                        },
                        'enabled' => 'admin/portfolio',
                    ],
                    'settings' => [
                        'class'   => 'pull-right',
                        'enabled' => 'admin/portfolio',
                    ]
                ],
                'sections' => [
                    'categories' => [
                        'buttons' => [
                            'new_category'
                        ],
                    ],
                    'fields' => [
                        'buttons' => [
                            'new_field' => [
                                'data-toggle' => 'modal',
                                'data-target' => '#modal',
                                'href'        => 'admin/portfolio/fields/choose',
                            ],
                        ],
                    ],
                    'settings' => [
                        'hidden'  => true,
                    ],
                    'assignments' => [
                        'hidden'  => true,
                        'href'    => 'admin/portfolio/assignments/{request.route.parameters.stream}',
                        'buttons' => [
                            'assign_fields' => [
                                'data-toggle' => 'modal',
                                'data-target' => '#modal',
                                'href'        => 'admin/portfolio/assignments/{request.route.parameters.stream}/choose',
                            ],
                        ],
                    ],
                ],
            ],
            'clients' => [
                'buttons' => [
                    'new_client',
                ],
            ],
            'services' => [
                'buttons' => [
                    'new_service',
                    'change_view' => [
                        'type'    => 'info',
                        'enabled' => 'admin/portfolio/services',
                        'icon'    => ($view == 'tree' ? 'fa fa-table' : 'list-ul'),
                        'href'    => 'admin/portfolio/services/change/' . ($view == 'tree' ? 'table' : 'tree'),
                        'text'    => 'newebtime.module.portfolio::button.' . ($view == 'tree' ? 'table_view' : 'tree_view'),
                    ],
                ],
            ],
        ]);
    }
}
