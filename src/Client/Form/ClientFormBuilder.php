<?php

namespace Newebtime\PortfolioModule\Client\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class ClientFormBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ClientFormBuilder extends FormBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'cancel',
    ];
}
