<?php

namespace Newebtime\PortfolioModule\Client\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Interface ClientRepositoryInterface
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
interface ClientRepositoryInterface extends EntryRepositoryInterface
{

}
