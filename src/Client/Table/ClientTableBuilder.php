<?php

namespace Newebtime\PortfolioModule\Client\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class ClientTableBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ClientTableBuilder extends TableBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $filters = [
        'search' => [
            'fields' => [
                'title'
            ]
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'edit',
        'view'
    ];

    /**
     * {@inheritdoc}
     */
    protected $actions = [
        'delete'
    ];
}
