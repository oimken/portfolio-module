<?php

namespace Newebtime\PortfolioModule\Client;

use Anomaly\Streams\Platform\Model\Portfolio\PortfolioClientsEntryModel;
use Newebtime\PortfolioModule\Client\Contract\ClientInterface;

/**
 * Class ClientModel
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ClientModel extends PortfolioClientsEntryModel implements ClientInterface
{

}
