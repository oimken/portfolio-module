<?php

namespace Newebtime\PortfolioModule\Client;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Newebtime\PortfolioModule\Client\Contract\ClientRepositoryInterface;

/**
 * Class ClientRepository
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ClientRepository extends EntryRepository implements ClientRepositoryInterface
{
    /**
     * @var ClientModel
     */
    protected $model;

    /**
     * Create a new ClientRepository instance.
     *
     * @param ClientModel $model
     */
    public function __construct(ClientModel $model)
    {
        $this->model = $model;
    }
}
