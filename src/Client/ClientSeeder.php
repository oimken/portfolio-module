<?php

namespace Newebtime\PortfolioModule\Client;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class ClientSeeder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ClientSeeder extends Seeder
{
    /**
     * @var ClientRepository
     */
    protected $clients;

    /**
     * ClientSeeder constructor.
     *
     * @param ClientRepository $clients
     */
    public function __construct(ClientRepository $clients)
    {
        $this->clients = $clients;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->clients->create(
            [
                'title'   => 'Jason Richardson',
                'slug'    => 'jason_richardson',
                'enabled' => true
            ]
        );
    }
}
