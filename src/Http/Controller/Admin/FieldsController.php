<?php

namespace Newebtime\PortfolioModule\Http\Controller\Admin;

/**
 * Class FieldsController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class FieldsController extends \Anomaly\Streams\Platform\Http\Controller\FieldsController
{
    /**
     * {@inheritdoc}
     */
    protected $namespace = 'portfolio';
}
