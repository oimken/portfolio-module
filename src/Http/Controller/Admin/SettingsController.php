<?php

namespace Newebtime\PortfolioModule\Http\Controller\Admin;

use Anomaly\SettingsModule\Setting\Form\SettingFormBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\Streams\Platform\Support\Authorizer;

/**
 * Class SettingsController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class SettingsController extends AdminController
{
    /**
     * Return a form for editing settings.
     *
     * @param  SettingFormBuilder $form
     * @param  Authorizer $authorizer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(SettingFormBuilder $form, Authorizer $authorizer)
    {
        if (!$authorizer->authorize('anomaly.module.settings::settings.write')) {
            abort(403);
        }

        $form->setButtons([
            'cancel' => [
                'href' => 'admin/portfolio'
            ]
        ]);

        return $form->render('newebtime.module.portfolio');
    }
}
