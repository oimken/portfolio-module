<?php

namespace Newebtime\PortfolioModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Illuminate\Routing\Redirector;
use Newebtime\PortfolioModule\Client\Contract\ClientRepositoryInterface;
use Newebtime\PortfolioModule\Client\Form\ClientFormBuilder;
use Newebtime\PortfolioModule\Client\Table\ClientTableBuilder;

/**
 * Class ClientsController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ClientsController extends AdminController
{
    /**
     * Display an index of existing entries.
     *
     * @param ClientTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ClientTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ClientFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ClientFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ClientFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ClientFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    /**
     * @param ClientRepositoryInterface $clients
     * @param Redirector $redirect
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function view(ClientRepositoryInterface $clients, Redirector $redirect, $id)
    {
        $client = $clients->find($id);

        return $redirect->to(route('newebtime.module.portfolio::customers.index', ['customerSlug' => $client->slug]));
    }
}
