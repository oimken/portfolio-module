<?php

namespace Newebtime\PortfolioModule\Http\Controller\Admin;

use Anomaly\PreferencesModule\Preference\Contract\PreferenceRepositoryInterface;
use Illuminate\Routing\Redirector;
use Newebtime\PortfolioModule\Service\Contract\ServiceRepositoryInterface;
use Newebtime\PortfolioModule\Service\Form\ServiceFormBuilder;
use Newebtime\PortfolioModule\Service\Table\ServiceTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Newebtime\PortfolioModule\Service\Tree\ServiceTreeBuilder;

/**
 * Class ServicesController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServicesController extends AdminController
{
    /**
     * Display an index of existing entries.
     *
     * @param ServiceTreeBuilder            $tree
     * @param ServiceTableBuilder           $table
     * @param PreferenceRepositoryInterface $preferences
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(
        ServiceTreeBuilder $tree,
        ServiceTableBuilder $table,
        PreferenceRepositoryInterface $preferences
    ) {
        if ($preferences->value('newebtime.module.portfolio::page_view', 'tree') == 'table') {
            return $table->render();
        }

        return $tree->render();
    }

    /**
     * @param PreferenceRepositoryInterface $preferences
     * @param                               $view
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change(PreferenceRepositoryInterface $preferences, $view)
    {
        $preferences->set('newebtime.module.portfolio::page_view', $view);

        return $this->redirect->back();
    }

    /**
     * Create a new entry.
     *
     * @param ServiceFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ServiceFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ServiceFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ServiceFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    /**
     * @param ServiceRepositoryInterface $services
     * @param Redirector $redirect
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function view(ServiceRepositoryInterface $services, Redirector $redirect, $id)
    {
        $service = $services->find($id);

        return $redirect->to(route('newebtime.module.portfolio::services.index', ['serviceSlug' => $service->slug]));
    }
}
