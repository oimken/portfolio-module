<?php

namespace Newebtime\PortfolioModule\Http\Controller\Admin;

/**
 * Class AssignmentsController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class AssignmentsController extends \Anomaly\Streams\Platform\Http\Controller\AssignmentsController
{
    /**
     * {@inheritdoc}
     */
    protected $namespace = 'portfolio';
}
