<?php

namespace Newebtime\PortfolioModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Routing\UrlGenerator;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;
use Illuminate\Database\Eloquent\Builder;
use Newebtime\PortfolioModule\Client\ClientRepository;
use Newebtime\PortfolioModule\Project\ProjectRepository;

/**
 * Class CustomersController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CustomersController extends PublicController
{
    /**
     * @var ClientRepository
     */
    protected $repository;

    /**
     * @var ProjectRepository
     */
    protected $projects;

    /**
     * ServicesController constructor.
     *
     * @param ClientRepository $repository
     * @param ProjectRepository $projects
     * @param BreadcrumbCollection $breadcrumbs
     * @param UrlGenerator $url
     */
    public function __construct(
        ClientRepository $repository,
        ProjectRepository $projects,
        BreadcrumbCollection $breadcrumbs,
        UrlGenerator $url
    ) {
        parent::__construct();

        $this->repository  = $repository;
        $this->projects    = $projects;
        $this->breadcrumbs = $breadcrumbs;
        $this->url         = $url;
    }

    /**
     * @param null $customerSlug
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index($customerSlug = null)
    {
        if ($customerSlug != null) {
            if (!$customer = $this->repository->newQuery()->where('slug', $customerSlug)->first()) {
                abort(404);
            }

            $projects = $this->projects->newQuery()
                ->where('client_id', $customer->id)
                ->where('enabled', true)
                ->whereHas('services', function (Builder $query) {
                    $query->where('related_id', '!=', null);
                })
                ->orderBy(setting_value('newebtime.module.portfolio::list_sort_order'), 'asc')
                ->get();
        } else {
            $projects = $this->projects->newQuery()
                ->where('enabled', true)
                ->whereHas('services', function (Builder $query) {
                    $query->where('related_id', '!=', null);
                })
                ->orderBy(setting_value('newebtime.module.portfolio::list_sort_order'), 'asc')
                ->get();
        }

        $this->breadcrumbs->add(
            'module::breadcrumb.portfolio',
            $this->url->route('newebtime.module.portfolio::services.index')
        );
        $this->breadcrumbs->add(
            'module::breadcrumb.client',
            $this->url->route('newebtime.module.portfolio::customers.index')
        );

        return $this->view->make('newebtime.module.portfolio::customers.customers', [
            'customers' => $this->repository->all(),
            'projects'  => $projects,
            'slug'      => $customerSlug
        ]);
    }
}
