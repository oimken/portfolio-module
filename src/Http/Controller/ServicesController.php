<?php

namespace Newebtime\PortfolioModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Routing\UrlGenerator;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;
use Illuminate\Database\Eloquent\Builder;
use Newebtime\PortfolioModule\Project\ProjectRepository;
use Newebtime\PortfolioModule\Service\ServiceRepository;

/**
 * Class ServicesController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServicesController extends PublicController
{
    /**
     * @var ServiceRepository
     */
    protected $repository;

    /**
     * @var ProjectRepository
     */
    protected $projects;

    /**
     * ServicesController constructor.
     *
     * @param ServiceRepository    $repository
     * @param ProjectRepository    $projects
     * @param BreadcrumbCollection $breadcrumbs
     * @param UrlGenerator         $url
     */
    public function __construct(
        ServiceRepository $repository,
        ProjectRepository $projects,
        BreadcrumbCollection $breadcrumbs,
        UrlGenerator $url
    ) {
        parent::__construct();

        $this->repository = $repository;
        $this->projects   = $projects;
        $this->breadcrumbs = $breadcrumbs;
        $this->url = $url;
    }

    /**
     * @param null $serviceSlug
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index($serviceSlug = null)
    {
        if ($serviceSlug != null) {
            if (!$service = $this->repository->newQuery()->where('slug', $serviceSlug)->first()) {
                abort(404);
            }

            $projects = $this->projects->newQuery()->where('enabled', true)
                ->whereHas('services', function (Builder $query) use ($service) {
                    $query->where('related_id', $service->id);
                })
                ->orderBy(setting_value('newebtime.module.portfolio::list_sort_order'), 'asc')
                ->get();
        } else {
            $projects = $this->projects->newQuery()->where('enabled', true)
                ->whereHas('services', function (Builder $query) {
                    $query->where('related_id', '!=', null);
                })
                ->orderBy(setting_value('newebtime.module.portfolio::list_sort_order'), 'asc')
                ->get();
        }

        $this->breadcrumbs->add(
            'module::breadcrumb.portfolio',
            $this->url->route('newebtime.module.portfolio::services.index')
        );

        return $this->view->make('newebtime.module.portfolio::services.services', [
            'services' => $this->repository->newQuery()->where('parent_id', null)->get(),
            'projects' => $projects,
            'slug'     => $serviceSlug
        ]);
    }
}
