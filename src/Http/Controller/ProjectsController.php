<?php

namespace Newebtime\PortfolioModule\Http\Controller;

use Anomaly\EditorFieldType\EditorFieldType;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Routing\UrlGenerator;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;
use Newebtime\PortfolioModule\Project\ProjectRepository;

/**
 * Class ProjectsController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectsController extends PublicController
{
    /**
     * @var ProjectRepository
     */
    protected $repository;

    /**
     * ServicesController constructor.
     *
     * @param ProjectRepository $projects
     * @param BreadcrumbCollection $breadcrumbs
     * @param UrlGenerator $url
     */
    public function __construct(
        ProjectRepository $projects,
        BreadcrumbCollection $breadcrumbs,
        UrlGenerator $url
    )
    {
        parent::__construct();

        $this->repository  = $projects;
        $this->breadcrumbs = $breadcrumbs;
        $this->url         = $url;
    }

    /**
     * @param $projectSlug
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show($projectSlug)
    {
        abort_if(!$project = $this->repository->findBySlug($projectSlug), 404);

        /** @var EditorFieldType $layout */
        $layout = $project->category->getFieldType('layout');

        $project->setContent($this->view->make($layout->getViewPath(), compact('project'))->render());

        $this->breadcrumbs->add(
            'module::breadcrumb.portfolio',
            $this->url->route('newebtime.module.portfolio::services.index')
        );
        $this->breadcrumbs->add(
            $project->getTitle(),
            $this->url->route('newebtime.module.portfolio::projects.show', ['projectSlug' => $projectSlug])
        );

        $this->template
            ->set('title', $project->title)
            ->set('meta_title', $project->meta_title ? $project->meta_title : $project->title)
            ->set('meta_description', $project->meta_description ? $project->meta_description : $project->title);

        return $this->view->make('newebtime.module.portfolio::projects.project', [
            'project' => $project,
        ]);
    }
}
