<?php

namespace Newebtime\PortfolioModule\Project;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class ProjectSeeder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectSeeder extends Seeder
{
    /**
     * @var ProjectRepository
     */
    protected $projects;

    /**
     * ProjectSeeder constructor.
     *
     * @param ProjectRepository $projects
     */
    public function __construct(ProjectRepository $projects)
    {
        $this->projects = $projects;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->projects->create(
            [
                'title'       => 'Spice Blends',
                'slug'        => 'spice_blends',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.',
                'industry'    => 'Adventure / Travel',
                'website'     => 'www.spiceblends.com',
                'client'      => 1,
                'enabled'     => true,
            ]
        );

        $this->projects->create(
            [
                'title'       => 'Filamento Lamps',
                'slug'        => 'filamento_lamps',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.',
                'industry'    => 'Lighting / Decorationl',
                'website'     => 'www.filamentolamps.com',
                'client'      => 1,
                'enabled'     => true,
            ]
        );

        $this->projects->create(
            [
                'title'       => 'Aura Herbals',
                'slug'        => 'aura_herbals',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.',
                'industry'    => 'Adventure / Travel',
                'website'     => 'www.auraherbals.com',
                'client'      => 1,
                'enabled'     => true,
            ]
        );

        $this->projects->create(
            [
                'title'       => 'Meat Bun',
                'slug'        => 'meat_bun',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                'industry'    => 'Adventure / Travel',
                'website'     => 'www.meatbun.com',
                'client'      => 1,
                'enabled'     => true,
            ]
        );
    }
}
