<?php

namespace Newebtime\PortfolioModule\Project\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Interface ProjectRepositoryInterface
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
interface ProjectRepositoryInterface extends EntryRepositoryInterface
{

}
