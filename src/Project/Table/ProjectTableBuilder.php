<?php

namespace Newebtime\PortfolioModule\Project\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;
use Newebtime\PortfolioModule\Project\Table\Filter\ServiceFilterOptions;

/**
 * Class ProjectTableBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectTableBuilder extends TableBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $filters = [
        'search' => [
            'fields' => [
                'title'
            ]
        ],
        'enabled',
        'services' => [
            'filter'  => 'select',
            'options' => ServiceFilterOptions::class,
        ]
    ];

    /**
     * {@inheritdoc}
     */
    protected $columns = [
        'title',
        'entry.enabled.toggle'  => [
            'is_safe' => true,
        ],
        'entry.images.first.preview',
        'entry.services.render()',
    ];

    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'edit',
        'view' => [
            'target' => '_blank',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    protected $actions = [
        'delete'
    ];
}
