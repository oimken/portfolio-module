<?php

namespace Newebtime\PortfolioModule\Project\Table\Filter;

use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Contract\SelectFilterInterface;
use Newebtime\PortfolioModule\Service\ServiceRepository;

/**
 * Class ServiceFilterOptions
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ServiceFilterOptions
{
    /**
     * Handle the options.
     *
     * @param SelectFilterInterface $filter
     * @param ServiceRepository     $services
     */
    public function handle(SelectFilterInterface $filter, ServiceRepository $services)
    {
        $filter->setOptions($services->newQuery()->where('parent_id', null)->get()->pluck('title', 'id')->all());
    }
}
