<?php

namespace Newebtime\PortfolioModule\Project;

use Newebtime\PortfolioModule\Project\Contract\ProjectRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class ProjectRepository
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectRepository extends EntryRepository implements ProjectRepositoryInterface
{
    /**
     * @var ProjectModel
     */
    protected $model;

    /**
     * Create a new ProjectRepository instance.
     *
     * @param ProjectModel $model
     */
    public function __construct(ProjectModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param $value
     *
     * @return ProjectModel|null
     */
    public function findBySlug($value)
    {
        return $this->model->where('slug', $value)->first();
    }
}
