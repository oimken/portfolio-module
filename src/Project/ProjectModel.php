<?php

namespace Newebtime\PortfolioModule\Project;

use Anomaly\Streams\Platform\Model\Portfolio\PortfolioProjectsEntryModel;
use Newebtime\PortfolioModule\Project\Command\GetStream;
use Newebtime\PortfolioModule\Project\Contract\ProjectInterface;

/**
 * Class ProjectModel
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectModel extends PortfolioProjectsEntryModel implements ProjectInterface
{
    /**
     * The project's content.
     *
     * @var null|string
     */
    protected $content = null;

    /**
     * {@inheritdoc}
     */
    public function getEntryStream()
    {
        return $this->dispatch(new GetStream($this));
    }

    /**
     * {@inheritdoc}
     */
    public function getEntryStreamId()
    {
        if (!$stream = $this->getEntryStream()) {
            return null;
        }

        return $stream->getId();
    }

    /**
     * @return null|string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param null|string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
}
