<?php

namespace Newebtime\PortfolioModule\Project;

use Anomaly\Streams\Platform\Entry\EntryPresenter;
use Carbon\Carbon;

/**
 * Class ProjectPresenter
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectPresenter extends EntryPresenter
{
    /**
     * @var ProjectModel
     */
    protected $object;

    /**
     * @return string
     */
    public function clientAndLabel()
    {
        return $this->transLabel('client') . $this->object->client->title;
    }

    /**
     * @return string
     */
    public function servicesAndLabel()
    {
        $services = '';

        foreach ($this->object->services as $key => $service) {
            if ($service->page_id) {
                $services .= '<a href="'. $service->page->path . '">' . $service->title . '</a>';
            } else {
                $services .= $service->title;
            }

            $services .= ($this->object->services->count() == ($key+1) ? '' : ', ');
        }

        return $this->transLabel('services') . '<span>' . $services . '</span>';
    }

    /**
     * @return string
     */
    public function dateAndLabel()
    {
        $date = new Carbon($this->object->date);

        return $this->transLabel('date') .
             '<span>' . $date->formatLocalized('%B') . ' ' . $date->day . ', ' . $date->year . '</span>';
    }

    /**
     * @return string
     */
    public function serviceTags()
    {
        $services = '';

        foreach ($this->object->services->where('parent_id', null) as $service) {
            $services .= '<span>' . $service->title . '</span>';
        }

        return $services;
    }

    /**
     * @param string $name
     * @param string $class
     *
     * @return string
     */
    public function li($name, $class = 'list-group-item')
    {
        if (!$this->object->$name || is_a($this->object->$name, 'ArrayAccess') && !$this->object->$name->count()) {
            return '';
        }

        $methodName = $name . 'AndLabel';

        return '<li class="' . $class . '">' .
             (method_exists($this, $methodName) ? $this->$methodName() : $this->labelAndValue($name)) .
            '</li>';
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function labelAndValue($name)
    {
        return $this->transLabel($name) . '<span>' . $this->object->$name . '</span>';
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function transLabel($name)
    {
        return '<label>' . trans('module::message.project.' . $name) . '</label>';
    }
}
