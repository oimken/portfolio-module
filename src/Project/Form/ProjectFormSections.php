<?php

namespace Newebtime\PortfolioModule\Project\Form;

use Newebtime\PortfolioModule\Project\ProjectModel;

/**
 * Class ProjectFormSections
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectFormSections
{
    /**
     * Handle the form sections.
     *
     * @param ProjectFormBuilder $builder
     * @param ProjectModel       $projects
     */
    public function handle(ProjectFormBuilder $builder, ProjectModel $projects)
    {
        $assignments = $projects->getAssignments();
        $extraFields = $assignments->notLocked()->fieldSlugs();

        $sections = [
            'entry'   => [
                'tabs' => [
                    'general' => [
                        'title'  => 'newebtime.module.portfolio::tab.general',
                        'fields' => $assignments
                            ->withoutFields(['images', 'order', 'category', 'theme_layout', 'meta_title', 'meta_description'])
                            ->locked()
                            ->fieldSlugs(),
                    ],
                    'images' => [
                        'title'  => 'newebtime.module.portfolio::tab.images',
                        'fields' => [
                            'images',
                        ],
                    ],
                    'seo' => [
                        'title'  => 'newebtime.module.portfolio::tab.seo',
                        'fields' => [
                            'meta_title',
                            'meta_description',
                        ],
                    ],
                    'options' => [
                        'title'  => 'newebtime.module.portfolio::tab.options',
                        'fields' => [
                            'order',
                            'category',
                            'theme_layout',
                        ],
                    ],
                ],
            ],
        ];

        if (!empty($extraFields)) {
            $sections['entry']['tabs']['fields'] = [
                'title'  => 'newebtime.module.portfolio::tab.extra',
                'fields' => $extraFields,
            ];
        }

        $builder->setSections($sections);
    }
}
