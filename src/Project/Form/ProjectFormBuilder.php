<?php

namespace Newebtime\PortfolioModule\Project\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class ProjectFormBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class ProjectFormBuilder extends FormBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'cancel',
    ];
}
