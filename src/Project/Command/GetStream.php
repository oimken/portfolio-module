<?php

namespace Newebtime\PortfolioModule\Project\Command;

use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Newebtime\PortfolioModule\Project\Contract\ProjectInterface;

/**
 * Class GetStream
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class GetStream
{
    /**
     * The page type instance.
     *
     * @var ProjectInterface
     */
    protected $project;

    /**
     * Create a new GetStream instance.
     *
     * @param ProjectInterface $project
     */
    public function __construct(ProjectInterface $project)
    {
        $this->project = $project;
    }

    /**
     * Handle the command.
     *
     * @param  StreamRepositoryInterface $streams
     * @return \Anomaly\Streams\Platform\Stream\Contract\StreamInterface|null
     */
    public function handle(StreamRepositoryInterface $streams)
    {
        return $streams->findBySlugAndNamespace('projects', 'portfolio');
    }
}
