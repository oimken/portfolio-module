<?php

namespace Newebtime\PortfolioModule\Category\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class CategoryFormBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CategoryFormBuilder extends FormBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'cancel',
    ];
}
