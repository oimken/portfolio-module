<?php

namespace Newebtime\PortfolioModule\Category\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class CategoryTableBuilder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CategoryTableBuilder extends TableBuilder
{
    /**
     * {@inheritdoc}
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * {@inheritdoc}
     */
    protected $actions = [
        'delete'
    ];
}
