<?php

namespace Newebtime\PortfolioModule\Category;

use Newebtime\PortfolioModule\Category\Contract\CategoryRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class CategoryRepository
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CategoryRepository extends EntryRepository implements CategoryRepositoryInterface
{

    /**
     * @var CategoryModel
     */
    protected $model;

    /**
     * Create a new CategoryRepository instance.
     *
     * @param CategoryModel $model
     */
    public function __construct(CategoryModel $model)
    {
        $this->model = $model;
    }

    public function findBySlug($slug)
    {
        return $this->model
            ->newQuery()
            ->where('slug', $slug)
            ->first();
    }
}
