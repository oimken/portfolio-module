<?php

namespace Newebtime\PortfolioModule\Category;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class CategorySeeder
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CategorySeeder extends Seeder
{
    /**
     * @var CategoryRepository
     */
    protected $categories;

    /**
     * CategorySeeder constructor.
     * @param CategoryRepository $categories
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->categories->truncate();

        $this->categories->create(
            [
                'en' => [
                    'name'        => 'Default',
                    'description' => 'Default description',
                ],
                'fr' => [
                    'name'        => 'Default',
                    'description' => 'Default description',
                ],
                'slug'         => 'default',
                'theme_layout' => 'theme::layouts/default.twig',
                'layout'       => <<<EOT
<div class="container project">
    <div class="title">
        <span>
            {{ project.serviceTags()|raw }}
        </span>
        <h1>{{ project.title }}</h1>
    </div>
    <div class="description clearfix">
        <ul class="list-group list-group-flush">
            {{ project.li('industry')|raw }}
            {{ project.li('services')|raw }}
            {{ project.li('date')|raw }}
            {{ project.li('website')|raw }}
            {{ project.li('client')|raw }}
        </ul>
        {% if project.headline.value %}
            <p><strong>{{ project.headline }}</strong></p>
        {% endif %}
        {{ project.description|raw }}
    </div>
    <div class="row images">
        {% for image in project.images %}
            <div class="col-md-6">
                {{ image.make.class('img-fluid').attr('alt', project.title)|raw }}
            </div>
        {% endfor %}
    </div>
</div>
EOT
            ]
        );
    }
}
