<?php

namespace Newebtime\PortfolioModule\Category;

use Anomaly\Streams\Platform\Model\Portfolio\PortfolioCategoriesEntryModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Newebtime\PortfolioModule\Category\Contract\CategoryInterface;
use Newebtime\PortfolioModule\Project\ProjectModel;

/**
 * Class CategoryModel
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class CategoryModel extends PortfolioCategoriesEntryModel implements CategoryInterface
{
    /**
     * {@inheritdoc}
     */
    protected $restricts = [
        'projects',
    ];

    /**
     * @return HasMany|ProjectModel[]
     */
    public function projects()
    {
        return $this->hasMany(ProjectModel::class, 'category_id');
    }
}
