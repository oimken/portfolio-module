<?php

$factory->define(
    Newebtime\PortfolioModule\Project\ProjectModel::class,
    function (Faker\Generator $faker) {
        return [
            'title' => $faker->title,
            'slug'  => $faker->slug,
        ];
    }
);
