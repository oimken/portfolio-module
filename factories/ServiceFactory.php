<?php

$factory->define(
    Newebtime\PortfolioModule\Service\ServiceModel::class,
    function (Faker\Generator $faker) {
        return [
            'title' => $faker->word,
            'slug'  => $faker->slug,
        ];
    }
);
