<?php

$factory->define(
    Newebtime\PortfolioModule\Category\CategoryModel::class,
    function (Faker\Generator $faker) {
        return [
            'name'   => $faker->word,
            'slug'   => $faker->slug,
            'layout' => '<h1>{{ project.title }}</h1>',
        ];
    }
);
