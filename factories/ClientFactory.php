<?php

$factory->define(
    Newebtime\PortfolioModule\Client\ClientModel::class,
    function (Faker\Generator $faker) {
        return [
            'title' => $faker->name,
            'slug'  => $faker->slug,
        ];
    }
);
