# Portfolio Module

Manage portfolio projects and clients

## Installation

`composer required newebtime/portfolio-module`

## Addons

The module come with 2 bundles addons

* portfolio_link_type-extension
* portfolio_projects_block-extension
