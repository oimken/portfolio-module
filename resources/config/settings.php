<?php

return [
    'columns' => [
        'type' => 'anomaly.field_type.integer',
        'config'   => [
            'default_value' => 3,
        ],
    ],
    'list_sort_order' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options' => [
                'title'      => 'Title',
                'created_at' => 'Created at',
                'updated_at' => 'Updated at',
                'order'      => 'Sort order'
            ],
            'default_value' => 'order'
        ]
    ],
    'show_project_intro' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options' => [
                'hide' => 'Hide',
                'show' => 'Show',
            ],
            'default_value' => 'hide'
        ]
    ],
    'layout_fluid' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options' => [
                'yes' => 'Yes',
                'no'  => 'No',
            ],
            'default_value' => 'no'
        ]
    ],
];
