<?php

return [
    'clients' => [
        'read',
        'write',
        'delete',
    ],
    'projects' => [
        'read',
        'write',
        'delete',
    ],
    'services' => [
        'read',
        'write',
        'delete',
    ],
    'categories' => [
        'read',
        'write',
        'delete',
    ],
];
