<?php

return [
    'page_view' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options' => [
                'tree'  => 'newebtime.module.portfolio::preferences.page_view.option.tree',
                'table' => 'newebtime.module.portfolio::preferences.page_view.option.table',
            ],
        ],
    ],
];
