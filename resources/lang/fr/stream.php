<?php

return [
    'categories' => [
        'name' => 'Catégories',
    ],
    'clients' => [
        'name' => 'Clients',
    ],
    'projects' => [
        'name'        => 'Projets',
        'description' => 'Votre porfolio de projets',
    ],
    'services' => [
        'name' => 'Services',
    ],
];
