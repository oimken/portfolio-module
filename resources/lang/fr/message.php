<?php

return [
    'project' => [
        'client'   => 'Client',
        'industry' => 'Industrie',
        'services' => 'Services',
        'date'     => 'Date',
        'website'  => 'Website'
    ],
];
