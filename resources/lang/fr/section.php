<?php

return [
    'categories' => [
        'title' => 'Catégories',
    ],
    'clients' => [
        'title' => 'Clients',
    ],
    'projects' => [
        'title' => 'Projets',
    ],
    'services' => [
        'title' => 'Services',
    ],
    'settings' => [
        'title' => 'Paramètres',
    ],
];
