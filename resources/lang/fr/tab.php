<?php

return [
    'general' => 'Général',
    'images'  => 'Images',
    'seo'     => 'SEO',
    'options' => 'Options',
    'extra'   => 'Extra'
];
