<?php

return [
    'title' => [
        'name' => 'Titre'
    ],
    'slug' => [
        'name' => 'Slug'
    ],
    'description' => [
        'name' => 'Description'
    ],
    'industry' => [
        'name' => 'Industrie'
    ],
    'website' => [
        'name' => 'Website'
    ],
    'date' => [
        'name' => 'Date'
    ],
    'client' => [
        'name' => 'Client'
    ],
    'services' => [
        'name' => 'Services'
    ],
    'images' => [
        'name' => 'Images'
    ],
    'enabled' => [
        'name' => 'Actif'
    ],
    'order' => [
        'name' => 'Ordre'
    ],
    'category' => [
        'name' => 'Categorie'
    ],
    'parent' => [
        'name' => 'Parent'
    ],
    'theme_layout' => [
        'name' => 'Theme Layout'
    ],
    'name' => [
        'name' => 'Nom'
    ],
    'layout' => [
        'name' => 'Layout'
    ],
    'meta_title' => [
        'name' => 'Meta Title'
    ],
    'meta_description' => [
        'name' => 'Meta Description'
    ],
    'logo' => [
        'name' => 'Logo'
    ],
    'page' => [
        'name' => 'Page'
    ],
    'headline' => [
        'name' => 'Gros titre'
    ],
];
