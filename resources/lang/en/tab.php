<?php

return [
    'general' => 'General',
    'images'  => 'Images',
    'seo'     => 'SEO',
    'options' => 'Options',
    'extra'   => 'Extra'
];
