<?php

return [
    'categories' => [
        'name' => 'Categories',
    ],
    'clients' => [
        'name' => 'Clients',
    ],
    'projects' => [
        'name'        => 'Projects',
        'description' => 'Your porfolio projects',
    ],
    'services' => [
        'name' => 'Services',
    ],
];
