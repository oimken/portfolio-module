<?php

return [
    'new_client'      => 'New Client',
    'new_project'     => 'New Project',
    'new_service'     => 'New Service',
    'new_category'    => 'New Category',
    'tree_view'       => 'Tree View',
    'table_view'      => 'Table View',
];
