<?php

return [
    'columns' => [
        'name' => 'Columns',
    ],
    'list_sort_order' => [
        'name' => 'List sort order',
    ],
    'show_project_intro' => [
        'name' => 'Show project intro',
    ],
    'layout_fluid' => [
        'name' => 'Layout fluid',
    ],
    'show_more_button' => [
        'name' => 'Show more button',
    ],
    'sort_order' => [
        'name' => 'Sort order',
    ],
    'project_link' => [
        'name' => 'Project link',
    ],
];
