<?php

return [
    'clients' => [
        'name'   => 'Clients',
        'option' => [
            'read'   => 'Can read clients?',
            'write'  => 'Can create/edit clients?',
            'delete' => 'Can delete clients?',
        ],
    ],
    'projects' => [
        'name'   => 'Projects',
        'option' => [
            'read'   => 'Can read projects?',
            'write'  => 'Can create/edit projects?',
            'delete' => 'Can delete projects?',
        ],
    ],
    'services' => [
        'name'   => 'Services',
        'option' => [
            'read'   => 'Can read services?',
            'write'  => 'Can create/edit services?',
            'delete' => 'Can delete services?',
        ],
    ],
    'categories' => [
        'name'   => 'Categories',
        'option' => [
            'read'   => 'Can read categories?',
            'write'  => 'Can create/edit categories?',
            'delete' => 'Can delete categories?',
        ],
    ],
];
