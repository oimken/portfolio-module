<?php

return [
    'categories' => [
        'title' => 'Categories',
    ],
    'clients' => [
        'title' => 'Clients',
    ],
    'projects' => [
        'title' => 'Projects',
    ],
    'services' => [
        'title' => 'Services',
    ],
    'settings' => [
        'title' => 'Settings',
    ],
];
