<?php

return [
    'title' => [
        'name' => 'Title'
    ],
    'slug' => [
        'name' => 'Slug'
    ],
    'description' => [
        'name' => 'Description'
    ],
    'industry' => [
        'name' => 'Industry'
    ],
    'website' => [
        'name' => 'Website'
    ],
    'date' => [
        'name' => 'Date'
    ],
    'client' => [
        'name' => 'Client'
    ],
    'services' => [
        'name' => 'Services'
    ],
    'images' => [
        'name' => 'Images'
    ],
    'enabled' => [
        'name' => 'Enabled'
    ],
    'order' => [
        'name' => 'Order'
    ],
    'category' => [
        'name' => 'Category'
    ],
    'parent' => [
        'name' => 'Parent'
    ],
    'theme_layout' => [
        'name' => 'Theme Layout'
    ],
    'name' => [
        'name' => 'Name'
    ],
    'layout' => [
        'name' => 'Layout'
    ],
    'meta_title' => [
        'name' => 'Meta Title'
    ],
    'meta_description' => [
        'name' => 'Meta Description'
    ],
    'logo' => [
        'name' => 'Logo'
    ],
    'page' => [
        'name' => 'Page'
    ],
    'headline' => [
        'name' => 'Headline'
    ],
];
