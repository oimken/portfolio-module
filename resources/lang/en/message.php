<?php

return [
    'project' => [
        'client'   => 'Client',
        'industry' => 'Industry',
        'services' => 'Services',
        'date'     => 'Date',
        'website'  => 'Website'
    ],
];
