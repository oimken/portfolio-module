<?php

return [
    'title'       => 'Portfolio',
    'name'        => 'Portfolio Module',
    'description' => '',
    'section'     => [
        'pages'       => 'Pages',
        'types'       => 'Types',
        'fields'      => 'Fields',
        'assignments' => 'Assignments',
    ],
];
